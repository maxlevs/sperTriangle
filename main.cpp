#include <windows.h>
#include <conio.h>
#define _USE_MATH_DEFINES
#include <cmath>

#define GREEN RGB(138, 179, 45)
#define   RED RGB(181,2,51)

/*Settings*/
#define  DELY 10
#define     N 6
#define   LEN 900
#define   AGL 0
#define COLOR GREEN


double getRad(double grad) {
	return (grad / 180) * M_PI;
}

double middle(double a, double b) {
	return (a - b) / 2 + b;
}

double geth(double a) {
	return  a * sqrt(3) / 2;
}

double* lineFrom(HDC hDC, double x, double y, double l, double angle) {
	Sleep(DELY);
	double dX = l*cos(angle), dY = l*sin(angle);
	double* res = new double[2];
	res[0] = x + dX;
	res[1] = y + dY;
	MoveToEx(hDC, x, y, NULL);
	LineTo(hDC, res[0], res[1]);
	return res;
}

double* triangle(HDC hDC, double x, double  y, double a, double angle = 0) {
	double* buff, *prev;
	double* bPoint = new double[4];

	angle += getRad(60);

	buff = lineFrom(hDC, x, y, a, angle);
	bPoint[0] = buff[0]; bPoint[1] = buff[1];

	prev = buff;
	buff = lineFrom(hDC, buff[0], buff[1], a, angle += getRad(120));
	delete[] prev;
	bPoint[2] = buff[0]; bPoint[3] = buff[1];

	prev = buff;
	buff = lineFrom(hDC, buff[0], buff[1], a, angle += getRad(120));
	delete[] buff; delete[] prev;

	return bPoint;
}

void cut(HDC hDC, double* PT, double a, size_t n, double angle = 0) {
	/* �������� ������� A ��� ���������� �������� [1][4] */
	double **A, **B;
	A = new double*[1];
	A[0] = new double[4];
	
	/* ������������� A ���������� ���������� */
	A[0][0] = PT[0]; A[0][1] = PT[1];
	A[0][2] = PT[2]; A[0][3] = PT[3];

	for (size_t i = 0; i < n; ++i) {
		B = new double*[pow(3, i+1)];
		a /= 2;
		for (int j = 0; j < pow(3, i); ++j) {
			/* ������ ������ ��� ����. ��������*/
			for(int k = 0; k < 3; ++k)
				B[3 * j + k] = new double[4]; // ����� �� ��������� ��� ���������� ���������? (+�������� ������ ��� ������� �������� � �) (!���������� ������������ ������ � ������������� ������ = ��������� ��������� � �������� �� ����)

				
			/* �������� ��� �������� */
			double D[2] = { 
				middle(A[j][0], A[j][2]), 
				middle(A[j][1], A[j][3])
			};
			double* buff = triangle(hDC, D[0], D[1], a, angle - getRad(180));
	
			/* ������ ���������� ��� j+1 � ������ �� � �*/
			B[3 * j + 0][0] = buff[0]; B[3 * j + 0][1] = buff[1];
			B[3 * j + 0][2] = buff[2]; B[3 * j + 0][3] = buff[3];

			B[3 * j + 1][0] = D[0];    B[3 * j + 1][1] = D[1];
			B[3 * j + 1][2] = A[j][0]; B[3 * j + 1][3] = A[j][1];

			B[3 * j + 2][0] = A[j][2]; B[3 * j + 2][1] = A[j][3];
			B[3 * j + 2][2] = D[0];    B[3 * j + 2][3] = D[1];
			
			delete[] buff;
			delete[] A[j];
		}
		delete[] A; // ����� �� ������� ��� ���������� ����������? (+�������� ������ ��� ������� �������� � �)
		A = B; // �������� ������ �� ����. ��������
	}

}

void main() {
	HWND hwnd;
	char Title[1024];
	GetConsoleTitle(Title, 1024);
	hwnd = FindWindow(NULL, Title);
	RECT rc;
	GetClientRect(hwnd, &rc);
	int iWidth = rc.right;
	int iHeight = rc.bottom;
	HDC hdc = GetDC(hwnd);
	HPEN p1, p2 = CreatePen(PS_SOLID, 2, COLOR);
	p1 = (HPEN)SelectObject(hdc, p2);

	double curX, curY;
	curX = (iWidth) / 2;
	curY = (iHeight - geth(LEN)) / 2;

	double* bPoint = triangle(hdc, curX, curY, LEN, getRad(AGL));
	cut(hdc, bPoint, LEN, N, getRad(AGL));

	SelectObject(hdc, p1);
	ReleaseDC(hwnd, hdc);
	DeleteObject(p2);
	_getch();
}